# README #

OpenCo Reverse Proxy extends jwilder/nginx-proxy to allow for big uploads.

### Requirements

* Bash
* Git 
* Docker

### How do I get set up? ###

* `git clone https://openco@bitbucket.org/openco/reverse-proxy.git` 
* `cd reverse-proxy.git`
* run `./Run.sh`

#### If you want to build your own image####
* `git clone https://openco@bitbucket.org/openco/reverse-proxy.git` 
* `cd reverse-proxy.git`
* run `./BuildnRun.sh`


### Who do I talk to? ###

* openco
* andrewjones